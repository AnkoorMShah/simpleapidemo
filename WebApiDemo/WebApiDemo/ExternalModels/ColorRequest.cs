﻿namespace WebApiDemo.ExternalModels
{
    public class ColorRequest
    {
        public string ColorText { get; set; }
        public string Action { get; set; }
    }
}
