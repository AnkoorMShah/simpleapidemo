﻿using System;
using System.Drawing;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using WebApiDemo.ExternalModels;

namespace WebApiDemo.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ColorController : ControllerBase
    {
        // fields
        private static Random RNGesus { get; }
        


        // constructors
        static ColorController()
        {
            RNGesus = new Random();
        }



        // Request handlers
        [HttpGet]
        public string GetRandomColor()
        {
            return string.Format("#{0:X6}", RNGesus.Next(0xFFFFFF + 1));
        }

        [HttpPost]
        public string PostConvertedColor(ColorRequest colorRequest)
        {
            string colorConversionResult = "Color Conversion Failed.";
            
            try
            {
                if ( colorRequest.Action == "hex")
                {
                    colorConversionResult = convertFromColorHex(colorRequest.ColorText);
                }
                else
                {
                    colorConversionResult = convertFromColorName(colorRequest.ColorText);
                }
            }
            catch { }

            return colorConversionResult;
        }

        private string convertFromColorName(string colorText)
        {
            return string.Format("#{0:X6}", Color.FromName(colorText).ToArgb() & 0xFFFFFF);
        }

        private string convertFromColorHex(string colorText)
        {
            int argb = Convert.ToInt32(colorText, 16);
            Color color = Color.FromArgb(argb);

            return color.ToString();
        }
    }
}